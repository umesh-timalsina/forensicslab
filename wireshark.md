## Wireless Packet Capture using Wireshark

Wireshark can be used to capture wireless network packets. Installing and configuring Wireshark for wireless traffic. Most of the wireless cards support monitor mode. In this lab, we will be looking into Wireshark, how to install wireshark in a linux platform, look at the GUI interface for wireshark for linux platform. I have a lenovo ThinkPad-T430s which has a wireless chipset that supports monitor mode. I will be setting up a new wireless interface for my chip in monitor mode that can be used to sniff wireless traffic. Now, 802.11 traffic sniffing can be hard because of various reasons. We will discuss that shortly. But before diving into wireless traffic capture with wireshark, we will look into installing wireshark for linux platform (Ubuntu 16.04 LTS), various features like filters, traffic capture functions etc.. Then we will dive into wireless networks. We will look into IEEE 802.11 wireless protocol, and radio frequencies, channels and other issues with frames and protocols of 802.11 networks. After that, we will be looking into challenges of capturing wireless traffic with wireshark. Finally, we will do a demo capture.

## Installing and running wireshark in Linux

In debians, aptitude package manager can be used to install wireshark. This can be done using following command. First of all, add a personal package repository for wireshark developers team 

```bash 
    $ sudo add-apt-repository ppa:wireshark-dev/stable
    $ sudo apt-get update
    $ sudo apt-get install wireshark
```

For linux geeks who want to bleed with software installation dependency wounds, I recommend installing wireshark from sources. The documentation is available [here](https://www.wireshark.org/docs/wsug_html_chunked/ChBuildInstallUnixBuild.html).

For some features of wireshark,sudo previlages are required in a linux environment. You can run wireshark using the following command:

```
    $ sudo wireshark &
```
When you run wireshark for the first time the following screen will appear:
![wireshark-home](images/ws1.png)

### Packet Capture

Most of the wireless chips and interfaces have a mode known as <code> promiscuous mode</code>. In promisocus mode, instead of returning the payload of the network packets and frames, the chip returns the whole packets. Wireshark captures packets by leveraging the promiscuous mode. To capture packets using wireshark, first of all we have to select an interface from which we want to capture packets. If *iw/ ifconfig* commands can be used to check the network interfaces available in linux. My interface is named wlan0. To start capturing, click on the interface wlan0. The screenshot below shows the packet capture. Use the red square button to stop the packet caputure. Hit <i>ctrl + s</i> to save the captured package to a file.
![wireshark-capture](images/ws2.png)

#### Display and Capture Filters 

Capture filters are applied before a capture. Capturing network traffic means getting about 100s or even more packets a second. So, applying appropriate filters for just what is needed is going to be of big help in real network capture scenarios. Capture filters are different from display filters in wireshark, we will look into display filters next. Basically, capture filters are used to reduce the size of a raw packet capture.  
The list below shows few example of capture filters:

* Capture only traffic from the ip address 131.230.21.36 (siu.edu has address 131.230.21.36):
```bash
   host 131.230.21.36
```

* capture traffic to or from a range of ip addresses:
```bash
    net 192.168.0.0/24
```

* Capture only IPV4 traffic
```bash
    ip
```

More information on capture filters can be found [here](https://www.wireshark.org/docs/wsug_html_chunked/ChCapCaptureFilterSection.html).

Display filters are more powerful. Wireshark provides a simple but powerful display filter language that allows you to build quite complex filter expressions. You can compare values in packets as well as combine expressions into more specific expressions. 

Few examples of display filters:  
* show only SMTP traffic:  
```
    tcp.port eq 25
```
* Show only traffic of LAN(192.168.x.x)
```
    ip.src==192.168.0.0/16 and ip.dst==192.168.0.0/16
```

## IEEE 802.11 Wireless Protocol
(Excerpts from www.networkworld.com)

They comprise a family of physical and datalink layer protocols designed to support wireless local area networks. In the world of wireless, the term Wi-Fi is synonymous with wireless access in general, despite the fact that it is a specific trademark owned by the Wi-Fi Alliance, a group dedicated to certifying that Wi-Fi products meet the IEEE’s set of 802.11 wireless standards. 

These standards, with names such as 802.11b (pronounced “Eight-O-Two-Eleven-Bee”, ignore the “dot”) and 802.11ac, comprise a family of specifications that started in the 1990s and continues to grow today. The 802.11 standards codify improvements that boost wireless throughput and range as well as the use of new frequencies as they  become available. They also address new technologies that reduce power consumption.

The names of these standards create quite an alphabet soup, made all-the-more confusing because they are not arranged alphabetically. To help clarify the situation, here’s an update on these physical-layer standards within 802.11, listed in reverse chronological order, with the newest standards at the top, and the oldest toward the bottom. After that is a description of standards that are still in the works.

### 802.11ah
Also known as Wi-Fi HaLow, 802.11ah defines operation of license-exempt networks in frequency bands below 1GHz (typically the 900 MHz band), excluding the TV White Space bands. In the U.S., this includes 908-928MHz, with varying frequencies in other countries. The purpose of 802.11ah is to create extended-range Wi-Fi networks that go beyond typical networks in the 2.4GHz and 5GHz space (remember, lower frequency means longer range), with data speeds up to 347Mbps. In addition, the standard aims to have lower energy consumption, useful for Internet of Things devices to communicate across long ranges without using a lot of energy. But it also could compete with Bluetooth technologies in the home due to its lower energy needs. The protocol was approved in September 2016 and published in May 2017.

### 802.11ad
Approved in December 2012, 802.11ad is very fast - it can provide up to 6.7Gbps of data rate across the 60 GHz frequency, but that comes at a cost of distance – you achieve this only if your client device is situated within 3.3 meters (only 11 feet) of the access point.

### 802.11ac
Current home wireless routers are likely  802.1ac-compliant, and operate in the 5 GHz frequency space. With Multiple Input, Multiple Output (MIMO) – multiple antennas on sending and receiving devices to reduce error and boost speed – this standard supports data rates up to 3.46Gbps. Some router vendors include technologies that support the 2.4GHz frequency via 802.11n, providing support for older client devices that may have 802.11b/g/n radios, but also providing additional bandwidth for improved data rates.

## 802.11n
The first standard to specify MIMO, 802.11n was approved in October 2009 and  allows for usage in two frequencies - 2.4GHz and 5GHz, with speeds up to 600Mbps. When you hear wireless LAN vendors use the term “dual-band”, it refers to being able to deliver data across these two frequencies.

### 802.11g as well
Approved in June 2003, 802.11g was the successor to 802.11b, able to achieve up to 54Mbps rates in the 2.4GHz band, matching 802.11a speed but within the lower frequency range.

### 802.11a
The first “letter” following the June 1997 approval of the 802.11 standard, this one provided for operation in the 5GHz frequency, with data rates up to 54Mbps. Counterintuitively, 802.11a came out later than 802.11b, causing some confusion in the marketplace because eople expected that the standard with the "b" at the end would be backward compatible with the one with the "a" at the end.

### 802.11b
Released in September 1999, it’s most likely that your first home router was 802.11b, which operates in the 2.4GHz frequency and provides a data rate up to 11 Mbps. Interestingly, 802.11a products hit the market before 802.11a, which was approved at the same time but didn’t hit the market until later.

### 802.11-1997
The first standard, providing a data rate up to 2 Mbps in the 2.4GHz frequency. It provided a range of a whopping 66 feet of indoors (330 feet outdoors), so if you owned one of these routers, you probably only used it in a single room.

### Challenges of Sniffing Wireless Traffic
First and foremost challenge of capturing wireless traffic with wireshark is to select which channel to capture traffic from. Unlike wired medium like ethernet, the wireless traffic can be distributed through various channels in different frequencies. Even if two wireless users are sitting side by side, chances are they might by communicating with the wireless access point in different channels. The decision to capture a particular channel resides on part of the administrator. If you  want  to  analyze  the  traffic  for  a specific  wireless AP  or  station, you  must identify  the  channel  or  frequency  used  by  the  target  device, and  configure  your wireless  card  to  use  the  same  channel  before  initiating your  packet capture. This  is  because  wireless  cards  can  only  operate  on  a single  frequency  at  any  given  time. If  you  wanted  to  capture  traffic  from  multiple  channels  simultaneously, you  would   need  an  additional  wireless  card  for  every  channel  you  wanted  to  monitor or use channel hopping. In channel hopping, you make your wireless card to rapidly switch between various channels and capture the packets in their entirety. However, this still does not guarantee that all the traffic is captured because of the channel switching overhead.

## Demo 1 : Switching to Monitor Mode and Capturing Wireless Traffic in Ubuntu Linux
All said and done, let us get our hands dirty. This time we will not be able to get our hands on some real interesting packets though. The demonstration I am about to show was be able to sniff traffic from my neighbors' access points as well and I did not want to try and decrypt their traffic. Beaware that decrypting other people's communication traffic can have some serious legal ramifications (Recommended usage in a sandboxed lab environment only).

### Step One: Switching to Monitor Mode using IW Command

There are different modes supported by the wireless. In *managed mode*, the wireless card and driver software rely on the local access point to provide connectivity. In *ad-hoc mode* , two wireless stations that want to communicate with each other can directly do so sharing the role of AP. Finally, wireless cards support *monitor mode* functionality. When configured in monitor mode, the wireless card stops transmitting and sniffs the currently configured channel, reporting contents of any observed packets to the host operating system. This  is the  most  useful  mode  of operation  for  analysis when  using Wireshark, because a wireless  card  configured  in monitor  mode  reports the  entire  contents  of wireless packets, including header information  and  the  encrypted  or unencrypted  data  contents. When  in monitor  mode, the  wireless  card and  driver reports  the  wireless frames "as-is," giving the  most  accurate view  of the  wireless  activity  for  the  selected  channel.

Switching to monitor mode is easy in any linux system. I will list the interfaces in my computer by using the new *iw* command.
```bash
    $ iw dev    
    phy#0
        Interface wlan0
                ifindex 11
                wdev 0x6
                addr 60:67:20:8a:aa:c9
                ssid NETGEAR16-5G
                type managed
                channel 153 (5765 MHz), width: 40 MHz, center1: 5755 MHz
        Interface phy0.mon
                ifindex 6
                wdev 0x2
                addr 60:67:20:8a:aa:c8
                type monitor
```
Let us check if my card supports the monitor mode:
```bash
    $ iw phy phy0 info | less
     ...
     Supported interface modes:
                 * IBSS
                 * managed
                 * AP
                 * AP/VLAN ￼
                 * monitor
        Band 1:
    ...
```

Since my physical card support monitor mode, let us go ahead add a new interface to the physical device in monitor mode. This is done using the following command:
```bash
    $ sudo iw phy phy0 interface add mon0 type monitor
```
Verify:
```bash
    $ sudo iw dev
```
We will capture with the mon0 interface, so you can delete the normal wlan0 interface and activate mon0 interface: 
```bash
    $ sudo iw dev wlan0 del
    $ sudo ifconfig mon0 up
```

### Use Channel Hopping Script 
I found a small channel hopping script that I will run in the background to quickly switch between all possible wireless channels. The script is shown below:
```bash
    #!/bin/bash
    # script channel_hopping.sh
    IFACE=mon0
    IEEE80211bg="1 2 3 4 5 6 7 8 9 10 11"
    IEEE80211bg_intl="$IEEE80211bg 12 13 14"
    IEEE80211a="36 40 44 48 52 56 60 64 149 153 157 161"
    IEEE80211bga="$IEEE80211bg $IEEE80211a" 
    IEEE80211bga_intl="$IEEE80211bg_intl $IEEE80211a" 

    while true; do
        for channel in $IEEE80211bga; do
            echo "switching to channel $channel"
            iwconfig $IFACE channel $channel
            sleep 1
        done
    done

```

## Step Two - Begin Capture
Run the script in background:
```bash
    $ sudo ./channel_hopping.sh 2&> file.txt &
```
Finally, also begin capturing in wireshark, you should see the interface you just created. Here's my screenshot of the capture:
![capture-monitor-mode](images/ws3.png)

Now stop the script once you are done capturing and restore your wlan interface in managed mode.
```bash
    $ sudo iw dev mon0 del
    $ sudo iw phy phy0 interface add wlan0 type managed
```