# More on Display Filters

As you probably know by now, display filters are probably the most important constructs that you can play around in wireshark. Wireshark uses display filters for general packet filtering while viewing and for its ColoringRules.

## Some Examples of display filters

1. Show only smtp traffic (TCP Port 25) and ICMP Traffic:

```
tcp.port eq 25 or icmp
```

2. Show only traffic in LAN (192.168.x.x) between workstations and servers -- no internet:

```
ip.src==192.168.0.0/16 and ip.dst==192.168.0.0/16
```

3. TCP buffer full -- _source is instructing Destination to stop sending data_

```
tcp.window_size == 0 && tcp.flags.reset != 1
```

Display filters allow wireshark users to concentrate on the packets they are actually interested in while hiding uninteresting packets. They allow you to select packets by:

* Protocol
* The presence of a field
* The values of fields
* A Comparison between fields etc...

In order to display packets based on the protocol type only, simply type the protocol in which you are interested in. All protocol and field names are entered in lowercase. Also, don’t forget to press enter after entering the filter expression.

## Display Filter Fields

Every field in the packet details pane can be used as a filter string, this will result in showing only packets where the filed exists. For example: The filter string: tcp will show all packets containing tcp protocol.

### Comparing Values
```
________________________________________________________
|English    | C- Like   | Example                       |
|-------------------------------------------------------|
| eq        |   ==      |   Equal ip.src == 10.0.0.5    |
|-------------------------------------------------------|
| ne        |   !=      |   Not Equal                   |
|-------------------------------------------------------|
| gt        |    >      | Greater than frame.len > 10   |
|-------------------------------------------------------|
| lt        |    <      | Less than. <code>             |
|-------------------------------------------------------|
| ge        |   >=      | Greater than or equal to      |
|-------------------------------------------------------|
| le        |   <=      | Less than or equal to         |
|-------------------------------------------------------|
| contains  |           | Protocol, field or slice      |
|           |           |    contains a value           |
|-------------------------------------------------------|
|  matches  |   ~       |   regex matching              |
|-------------------------------------------------------|
|bitwise_and|   &       |   Compare bit field value     |
---------------------------------------------------------
```

### Display Filter Field Types

### Unsigned and Signed Integers

Can be 8, 16, 24, 32, or 64 bits. You can express integers in decimal, octal, or hexadecimal. The following display filters are equivalent:

```
ip.len le 1500
ip.len le 02734
ip.len le 0x5dc
```

#### Boolean

A boolean field is present in the protocol decode only if its value is true. For example, <code>tcp.flags.syn</code> is present, and thus true, only if the SYN flag is present in a TCP segment header.

#### Ethernet address

6 bytes separated by a colon (:), dot (.) or dash (-) with one or two bytes between separators:

```
eth.dst == ff:ff:ff:ff:ff:ff
eth.dst == ff-ff-ff-ff-ff-ff
eth.dst == ffff.ffff.ffff
```

#### IPV4 Addresses

```
ip.addr == 192.168.0.1
```

#### IPv6 address
```
ipv6.addr == ::1
```

#### Text String
```
http.request.uri == "https://www.wireshark.org/"
```

```
udp contains 81:60:03
```
The example above match packets that contains the 3-byte sequence 0x81, 0x60, 0x03 anywhere in the UDP header or payload.

```
http.host matches "acme\.(org|com|net)"
```
The example above match HTTP packets where the HOST header contains acme.org or acme.com or acme.net. Comparisons are case-insensitive.

```
tcp.flags & 0x02
```
That expression will match all packets that contain a “tcp.flags” field with the 0x02 bit, i.e. the SYN bit, set.

### Combining Expressions
You can combine filter expressions in Wireshark using the logical operators shown below:
![](./images/wireshark_continued_1.png)

### Slice Operator
Wireshark allows you to select sub-sequences of a sequence in rather elaborate ways. After a label you can place a pair of brackets [] containing a comma separated list of range specifiers.

```
eth.src[0:3] == 00:00:83
```
The example above uses the n:m format to specify a single range. In this case n is the beginning offset and m is the length of the range being specified.

```
eth.src[1-2] == 00:83
```
The example above uses the n-m format to specify a single range. In this case n is the beginning offset and m is the ending offset.

```
eth.src[0:3,1-2,:4,4:,2] ==
00:00:83:00:83:00:00:83:00:20:20:83
```

Wireshark allows you to string together single ranges in a comma separated list to form compound ranges as shown above.

### Membership Operator

Wireshark allows you to test a field for membership in a set of values or fields. After the field name, use the in operator followed by the set items surrounded by braces {}.
```
tcp.port in {80 443 8080}
```
This can be considered a shortcut operator, as the previous expression could have been expressed as:
```
tcp.port == 80 || tcp.port == 443 || tcp.port == 8080
```
The set of values can also contain ranges:
```
tcp.port in {443 4430..4434}
```
This is not merely a shortcut for tcp.port == 443 || (tcp.port >= 4430 && tcp.port <= 4434). Comparison operators are usually satisfied when any field matches the filter, and thus a packet with ports 80 and 56789 would match this alternative display filter since 56789 >= 4430 && 80 <= 4434 is true. The membership operator instead tests the same field against the range condition.

Sets are not just limited to numbers, other types can be used as well:
```
http.request.method in {"HEAD" "GET"}
ip.addr in {10.0.0.5 .. 10.0.0.9 192.168.1.1..192.168.1.9}
frame.time_delta in {10 .. 10.5}
```