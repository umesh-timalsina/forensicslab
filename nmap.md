## Introduction to Nmap

Nmap, or Network Mapper, is a free, open source tool that is available under the GNU 
General Public License as published by the Free Software Foundation. It is most often 
used by network administrators and IT security professionals to scan enterprise networks, 
looking for live hosts, specific services, or specific operating systems. Part of the beauty 
of Nmap is its ability to create IP packets from scratch and send them out utilizing 
unique methodologies to perform the above-mentioned types of scans and more. In addition, 
Nmap comes with command-line or GUI functionality and is easily installed on everything 
from Unix and Windows to Mac OS X. Installation requirements are dependent on the Nmap 
version you are installing and consist mainly of network library dependencies specific
to that version.


## Basic Scan Types

The two basic scan types used most in Nmap are TCP connect() scanning [-sT]
and SYN scanning (also known as half-open, or stealth scanning) [-sS].
These two types are explained in detail below:

### TCP connect() Scan [-sT]

These scans are so called because UNIX sockets programming uses a system call named
<code>connect()</code> to begin a TCP connection to a remote site. If <code>connect()</code>
succeeds, a connection was made. If it fails, the connection could not be made
(the remote system is offline, the port is closed, or some other error occurred
along the way). This allows a basic type of port scan, which attempts to connect 
to every port in turn, and notes whether or not the connection succeeded. Once the scan is completed, 
ports to which a connection could be established are listed as open, the rest are said to be closed.

This method of scanning is very effective, and provides a clear picture of the
ports you can and cannot access. If a connect() scan lists a port as open, you
can definitely connect to it - that is what the scanning computer just did! There
is, however, a major drawback to this kind of scan; it is very easy to detect on
the system being scanned. If a firewall or intrusion detection system is running
on the victim, attempts to connect() to every port on the system will almost
always trigger a warning. Indeed, with modern firewalls, an attempt to connect
to a single port which has been blocked or has not been specifically ”opened” will
usually result in the connection attempt being logged. Additionally, most servers
will log connections and their source IP, so it would be easy to detect the source
of a TCP connect() scan. For this reason, the TCP Stealth Scan was developed.

### SYN Sleath Scan [-sS]

When a TCP connection is made between two systems, a process known as a
”three way handshake” occurs. This involves the exchange of three packets, and
synchronises the systems with each other (necessary for the error correction built
into TCP. Refer to a good TCP/IP book for more details.
The system initiating the connection sends a packet to the system it wants
to connect to. TCP packets have a header section with a flags
field. Flags tell the receiving end something about the type of packet, and thus what the correct
response is.

Here, I will talk about only four of the possible flags. These are SYN (Syn-chronise), ACK (Acknowledge), FIN (Finished) and RST (Reset). 
SYN packets include a TCP sequence number, which lets the remote system know what sequence numbers to expect in subsequent communication. 
ACK acknowledges receipt of a packet or set of packets, FIN is sent when a communication is finished, requesting that the connection be closed, 
and RST is sent when the connection is to be reset (closed immediately). To initiate a TCP connection, the initiating system sends a SYN packet to
the destination, which will respond with a SYN of its own, and an ACK, acknowledging the receipt of the first packet (these are combined into a 
single SYN/ACK packet). The first system then sends an ACK packet to acknowledge receipt of
the SYN/ACK, and data transfer can then begin. SYN or Stealth scanning makes use of this procedure by sending a SYN packet
and looking at the response. If SYN/ACK is sent back, the port is open and the remote end is trying to open a TCP
connection. The scanner then sends an RST to tear down the connection before it can be established fully; often
preventing the connection attempt appearing in application logs. If the port is closed, an RST will be sent. 
If it is filtered, the SYN packet will have been dropped and no response will be sent. 
In this way, Nmap can detect three port states - open, closed and filtered. 
Filtered ports may require further probing since they could be subject to firewall rules which render them open to some IPs or conditions,
and closed to others.

## FIN, Null and Xmas Tree Scans [-sF, -sN, -sX]
With the multitude of modern firewalls and IDS’ now looking out for SYN scans,
these three scan types may be useful to varying degrees. Each scan type refers
to the flags set in the TCP header. The idea behind these type of scans is that
a closed port should respond with an RST upon receiving packets, whereas an
open port should just drop them (it’s listening for packets with SYN set). This
way, you never make even part of a connection, and never send a SYN packet;
which is what most IDS’ look out for.

The results are, predictably, the same, but the FIN scan is less
likely to show up in a logging system.


## Ping Scan [-sP]

This scan type lists the hosts within the specified range that responded to a ping.
It allows you to detect which computers are online, rather than which ports are
open. 

## UDP Scan [-sU]
Scanning for open UDP ports is done with the -sU option. With this scan type,
Nmap sends 0-byte UDP packets to each target port on the victim. Receipt of
an ICMP Port Unreachable message signifies the port is closed, otherwise it is
assumed open.

## IP Protocol Scans [-sO]
The IP Protocol Scans attempt to determine the IP protocols supported on a
target. Nmap sends a raw IP packet without any additional protocol header, 
to each protocol on the target machine. Receipt of an ICMP Protocol Unreachable 
message tells us the protocol is not in use, otherwise it is assumed open. 
Not all hosts send ICMP Protocol Unreachable messages. These may include firewalls, 
AIX, HP-UX and Digital UNIX). These machines will report all protocols open.

This scan type also falls victim to the ICMP limiting rate described in the
UDP scans section, however since only 256 protocols are possible (8-bit field for
IP protocol in the IP header) it should not take too long.
Results of an -sO on my Linux workstation are included below.

```bash
$ sudo nmap -sO 127.0.0.1

starting Nmap 7.01 ( https://nmap.org ) at 2018-09-27 12:50 CDT
Nmap scan report for localhost (127.0.0.1)
Host is up (0.00017s latency).
Not shown: 248 closed protocols
PROTOCOL STATE         SERVICE
1        open          icmp
2        open          igmp
6        open          tcp
17       open          udp
103      open          pim
136      open          udplite
252      open          unknown
255      open|filtered unknown

Nmap done: 1 IP address (1 host up) scanned in 14.57 seconds
```
## Version Detection [-sV]
Version Detection collects information about the specific service running on an
open port, including the product name and version number. This information can
be critical in determining an entry point for an attack. The -sV option enables
version detection, and the -A option enables both OS fingerprinting and version
detection, as well as any other advanced features which may be added in future
releases.

## ACK Scan [-sA]
Usually used to map firewall rulesets and distinguish between stateful and state-
less firewalls, this scan type sends ACK packets to a host. If an RST comes back,
the port is classified ”unfiltered” (that is, it was allowed to send its RST through
whatever firewall was in place). If nothing comes back, the port is said to be
”filtered”. That is, the firewall prevented the RST coming back from the port.
This scan type can help determine if a firewall is stateless (just blocks incoming
SYN packets) or stateful (tracks connections and also blocks unsolicited ACK packets).

## Timing and Hiding Scans

### Timing
Nmap adjusts its timings automatically depending on network speed and response
times of the victim. However, you may want more control over the timing in order
to create a more stealthy scan, or to get the scan over and done with quicker.
The main timing option is set through the -T parameter. There are six pre-
defined timing policies which can be specified by name or number (starting with
0, corresponding to Paranoid timing). The timings are Paranoid, Sneaky, Polite,
Normal, Aggressive and Insane. 
A -T Paranoid (or -T0) scan will wait (generally) at least 5 minutes between
each packet sent. This makes it almost impossible for a firewall to detect a port
scan in progress (since the scan takes so long it would most likely be attributed
to random network traffic). Such a scan will still show up in logs, but it will be
so spread out that most analysis tools or humans will miss it completely.
A -T Insane (or -T5) scan will map a host in very little time, provided you
are on a very fast network or don’t mind losing some information along the way.

## Basic Scanning Techniques example on scanme.nmap.org